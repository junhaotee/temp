#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
//#include <ctime>// include this header 


using namespace std;

void initArray(int arr[], int length);

void initArray(int arr[], int length){
	srand(time(NULL));
	int i=0;
	for(i=0;i<length;i++){
		arr[i]=rand()%100;
	}
	

}
int main(int argc, int *argv[]) {

	clock_t tStart = clock();	
	
	int arrSize=999999;

	int i, intArray[arrSize];
	int sum = 0;

	/* Store some values in intArray. */
	initArray(intArray, arrSize);
	
	#pragma parallel for /* Make the for loop a parallel region */
	#pragma threadprivate(i) /* The counter i is passed by value to each threads */
	#pragma reduction(+: sum) /* Telling openmp to join the 'sum'  */
	for (i=0;i<arrSize;i++) {
		sum = sum + intArray[i];
	}

	printf("%i",sum);

	printf("Time taken: %.9fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);





}
